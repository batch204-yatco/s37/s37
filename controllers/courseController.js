const Course = require("../models/Course");


// Create a New Course
module.exports.addCourse = (reqBody) => {
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price

	});

	return newCourse.save().then((course, error) => {
		if(error){
			return false
		} else {
			return true
		}
	});
}

// Retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

// Controller for retrieving all ACTIVE courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then( result => {
		return result;
	})
}

// Retrieving specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {return result
	})
}

// Updating a Course
module.exports.updateCourse = (reqParams, reqBody, data) => {

	if(data.isAdmin === true){
		let updatedCourse = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		};

		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	
	} else {

	return false
	
	}
} 

// Archiving a Course
module.exports.archiveCourse = (reqParams, reqBody, data) => {

	if(data.isAdmin === true){
		let archivedCourse = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			isActive: reqBody.isActive
		};

		return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {
			if(error){
				return false
			} else {
				return 'course archived'
			}
		})
	
	} else {

	return false
	
	}
}