const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI";

// JSON Web Tokens

// Token Creation 
module.exports.createAccessToken = (user) => {

	console.log(user)
	/*
		{
		  _id: new ObjectId("634015b3b80ce95b6dc704a5"),
		  firstName: 'Jane',
		  lastName: 'Hufano',
		  email: 'janehufano@gmail.com',
		  password: '$2b$10$mR3XUn64sPTWaodrYr7T2.rSXHJPOKQIbbFMVr9ZeUsleVLpG/UOO',
		  isAdmin: true,
		  mobileNo: '09123456789',
		  enrollments: [],
		  __v: 0
		}
	*/

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	// Generate a token
		// Generates JSON web token using sign method
		// jwt.sign(<payload>)
	return jwt.sign(data, secret, {})
}

// Token Verification
module.exports.verify = (req, res, next) => {

	// Contains bearer token
	let token = req.headers.authorization

	if (typeof token !== "undefined"){
		console.log(token)

		// removes word "Bearer"
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return res.send({auth: "failed"})
			} else {
				// finish auth.verify on userRoutes and move to (req, res)
				next()
			}
		})

	} else {
		return res.send({auth: "failed"});
	}
} 


// Token Decryption
module.exports.decode = (token) => {

	if(typeof token !== "undefined"){
		
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload
			}
		})
	} else {
		return null
	}
}