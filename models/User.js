const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		require: [true, "First name is required"]
	},
	lastName: {
		type: String,
		require: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"] 	
	},
	isAdmin: {
		type: Boolean,
		default: true
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile Number is required"] 
	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "Course Id is required"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Enrolled"
			}
		}
	]

});

module.exports = mongoose.model("User", userSchema);